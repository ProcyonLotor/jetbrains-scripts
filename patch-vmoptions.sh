#!/bin/sh

dirpath="$(cd "$(dirname "$(realpath "$0")")" && pwd)"
agent_jar="$dirpath/ja-netfilter/ja-netfilter.jar"

if [ ! -e "$agent_jar" ]; then
    echo "** ERROR: Java agent \"$(basename "$agent_jar")\" not found in \"$(dirname "$agent_jar")\""
    exit 1
fi

for prog in "$@"; do
    ide_home="$(cd "$prog" && pwd)"
    if [ -z "$ide_home" ]; then
        echo "** ERROR: Directory \"$prog\" doesn't exist"
        exit 1
    fi

    echo ""
    echo "Working on \"$ide_home\"..."
    for opsfile in "$ide_home/bin"/*.vmoptions; do
        if [ -e "$opsfile" ]; then
            opsfile_copy="${opsfile}.orig"
            if [ ! -e "$opsfile_copy" ]; then
                cp "$opsfile" "$opsfile_copy" || exit $?
            else
                cp -f "$opsfile_copy" "$opsfile" || exit $?
            fi
            echo "Patching \"$opsfile\""
            {
                echo "-javaagent:$agent_jar=jetbrains"
                echo "--add-opens=java.base/jdk.internal.org.objectweb.asm=ALL-UNNAMED"
                echo "--add-opens=java.base/jdk.internal.org.objectweb.asm.tree=ALL-UNNAMED"
            }>>"$opsfile"
        fi
    done
done

echo ""
echo "Done."
