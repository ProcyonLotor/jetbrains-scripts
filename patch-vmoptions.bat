@ECHO OFF
TITLE Java Agent patcher
SETLOCAL EnableDelayedExpansion

:: Get absolute path to agent .jar
SET "AGENT_JAR=%~dp0ja-netfilter\ja-netfilter.jar"

IF NOT EXIST "%AGENT_JAR%" (
	ECHO ** ERROR: Java agent "%AGENT_JAR%" not found
	GOTO end
)

:loop
IF NOT "%~1"=="" (
	SET "IDE_HOME=%~dpnx1"
	IF NOT EXIST "!IDE_HOME!" (
		ECHO ** ERROR: Directory "%~1" doesn't exist
		GOTO end
	)

	ECHO.
	ECHO Working on "!IDE_HOME!"...
	FOR %%I IN ("!IDE_HOME!\bin\*.vmoptions") DO (
		SET "OPSFILE=%%~I"
		SET "OPSFILE_COPY=!OPSFILE!.orig"
		IF NOT EXIST "!OPSFILE_COPY!" (
			COPY "!OPSFILE!" "!OPSFILE_COPY!" >NUL || GOTO end
		) ELSE (
			COPY /Y "!OPSFILE_COPY!" "!OPSFILE!" >NUL || GOTO end
		)

		ECHO Patching "!OPSFILE!"
		>>"!OPSFILE!" (
			ECHO -javaagent:%AGENT_JAR%=jetbrains
			ECHO --add-opens=java.base/jdk.internal.org.objectweb.asm=ALL-UNNAMED
			ECHO --add-opens=java.base/jdk.internal.org.objectweb.asm.tree=ALL-UNNAMED
		)
	)
	SHIFT & GOTO loop
)

ECHO.
ECHO Done.

:end
PAUSE
EXIT /B
