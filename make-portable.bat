@ECHO OFF
TITLE Portabilizer
SETLOCAL EnableDelayedExpansion

:loop
IF NOT "%~1"=="" (
	SET "IDE_HOME=%~dpnx1"
	IF NOT EXIST "!IDE_HOME!" (
		ECHO ** ERROR: Directory "%~1" doesn't exist
		GOTO end
	)

	ECHO.
	ECHO Working on "!IDE_HOME!"...

	SET "PROPSFILE=!IDE_HOME!\bin\idea.properties"
	IF NOT EXIST "!PROPSFILE!" (
		ECHO ** ERROR: File "!PROPSFILE!" doesn't exist
		GOTO end
	)

	SET "PROPSFILE_COPY=!PROPSFILE!.orig"
	IF NOT EXIST "!PROPSFILE_COPY!" (
		COPY "!PROPSFILE!" "!PROPSFILE_COPY!" >NUL || GOTO end
	) ELSE (
		COPY /Y "!PROPSFILE_COPY!" "!PROPSFILE!" >NUL || GOTO end
	)

	ECHO Patching "!PROPSFILE!"
	powershell -Command "(Get-Content '!PROPSFILE!').Replace('# idea.', 'idea.').Replace('idea.system.path=', '#idea.system.path=').Replace('user.home', 'idea.home.path') | Set-Content '!PROPSFILE!'"
	SHIFT & GOTO loop
)

ECHO.
ECHO Done.

:end
PAUSE
EXIT /B
