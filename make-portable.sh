#!/bin/sh

for prog in "$@"; do
    ide_home="$(cd "$prog" && pwd)"
    if [ -z "$ide_home" ]; then
        echo "** ERROR: Directory \"$prog\" doesn't exist"
        exit 1
    fi

    echo ""
    echo "Working on \"$ide_home\"..."

    propsfile="$ide_home/bin/idea.properties"
    if [ ! -e "$propsfile" ]; then
        echo "** ERROR: File \"$propsfile\" doesn't exist"
        exit 1
    fi

    propsfile_copy="${propsfile}.orig"
    if [ ! -e "$propsfile_copy" ]; then
        cp "$propsfile" "$propsfile_copy" || exit $?
    else
        cp -f "$propsfile_copy" "$propsfile" || exit $?
    fi

    echo "Patching \"$propsfile\""
    sed -i -e "s/# idea./idea./g" "$propsfile" || exit $?
    sed -i -e "s/idea.system.path=/#idea.system.path=/g" "$propsfile" || exit $?
    sed -i -e "s/user.home/idea.home.path/g" "$propsfile" || exit $?
done

echo ""
echo "Done."
